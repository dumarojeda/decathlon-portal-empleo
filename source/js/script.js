//= require jquery
//= require jquery-validate
//= require_tree .
$(document).on('ready', function(){
  menuOffCanvas();
  forms();
  submenus();
  search();
  dropdown();
  inputCustom();
  resizeDiv();
  validateForm();
  //validateForm('.form-login');
  //validateForm('.form-singup');
  function openFile(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
      var dataURL = reader.result;
      var output = document.getElementById('output');
      output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
  };
});