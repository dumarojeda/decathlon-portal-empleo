function otherfn (classOrIDForm) {
  $(classOrIDForm + ' button').click(function(e){

    e.preventDefault();
    $(classOrIDForm + ' input').each(function(){
      if(!isNaN(this.value)) {
        $(this).parent().addClass('error');
      } else {
        $(this).parent().removeClass('error');
        $(classOrIDForm + ' button').submit();
      }
    });

  });
}

function validateForm() {

  $('.form-login').validate();
  $('.form-singup').validate();


  jQuery.extend(jQuery.validator.messages, {
    required: "Campo requerido!",
  });
  jQuery.validator.addClassRules( "required", {
    required: true
  });


}