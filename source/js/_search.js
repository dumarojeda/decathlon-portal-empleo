function search() {
  $('.search-item .btn').on('click', function () {
    $('.search-item .field').toggleClass('active');
  });

  $("#wrapper-content").on("click", removeClassSearch);
  $("#wrapper-footer").on("click", removeClassSearch);

  function removeClassSearch () {
    $('.search-item .field').removeClass('active');
  }
}