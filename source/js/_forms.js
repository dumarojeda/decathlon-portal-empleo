function forms() {
  $('.radio input[type="radio"]').click(function() {
      $(this).parent().parent().find('.field.radio').find('label').removeClass('checked');
    if ($(this).is(':checked')) {
      $(this).parent().find('label').addClass('checked');
    } else {
      $(this).parent().find('label').removeClass('checked');
    }
  });

  $('.checkbox input[type="checkbox"]').click(function() {
    $(this).toggleClass('checked');
    if ($(this).is(':checked')) {
      $(this).parent().find('label').addClass('checked');
    } else {
      $(this).parent().find('label').removeClass('checked');
    }
  });

  $(".radio-question").change(function(){
    if($(this).is(":checked")){
      radioOption = $(this).parent().parent().parent().next().find("input.radio-option"); 
      fieldsContainer = $(this).parent().parent().parent().next().find(".fields"); 
      if($(this).val() == "si" || $(this).val() == "media") {   
        radioOption.removeAttr("disabled");
        fieldsContainer.removeClass("disabled");
      } else {
        radioOption.attr("disabled", "disabled");
        radioOption.parent().parent().addClass("disabled");
        radioOption.prop('checked', false); 
        radioOption.parent().find("label").removeClass("checked");
      }
    }
  }); 
}