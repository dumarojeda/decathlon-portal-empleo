window.onresize = function(event) {
  resizeDiv();
}

function resizeDiv() {
  vph = $(window).height();
  heightFooter = $('#wrapper-footer').height();
  heightHeader = $('#wrapper-header').height();
  heightauto = vph - (heightFooter/2 + heightHeader);
  $('.heightauto').css({'height': heightauto + 'px'});
  //Menu Fixes
  $(window).bind('scroll', function () {
    if ($(window).scrollTop() > vph / 2) {
      $('#menu-fixed').addClass('active');
    } else {
      $('#menu-fixed').removeClass('active');
    }
  });
  $('.topUp').click(function(e){
    e.preventDefault();
    $("html, body").animate({ scrollTop: 0 }, 'slow');
  });
}